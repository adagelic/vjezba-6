package com.example.vjezba6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.koushikdutta.ion.Ion;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // napravi objekt shared preferences pod svojim key-em
        SharedPreferences mySharedPreferences = this.getSharedPreferences(
                "POSTAVKE",
                Context.MODE_PRIVATE
        );

        // proÄitaj koliko se do sada puta activity kreirao
        int numberOfLoads = mySharedPreferences.getInt("broj_otvaranja", 0);

        // spremi novi broj kreiranja activity-a
        mySharedPreferences
                .edit()
                .putInt("broj_otvaranja", ++numberOfLoads)
                .apply() // .commit() pise odmah, .apply pise u pozadini
        ;

        // prikazi vrijednost u toast-u
        Toast.makeText(
                getApplicationContext(),
                "Broj kreiranja: " + String.valueOf(numberOfLoads),
                Toast.LENGTH_SHORT
        ).show();

        // postavi sliku sa URL-a na image view koristeÄ‡i ION
        ImageView myImageView = (ImageView) findViewById(R.id.imageView);
        Ion.with(myImageView).load(
                "http://wanna-joke.com/wp-content/uploads/2014/03/funny-picture-software-developer-song.jpg"
        );

        // Toastaj spinner vrijednost
        final Spinner mySpinner = (Spinner) findViewById(R.id.spinner);

        mySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(
                        getApplicationContext(),
                        "Selected country: " + mySpinner.getSelectedItem().toString(),
                        Toast.LENGTH_SHORT
                ).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
}
